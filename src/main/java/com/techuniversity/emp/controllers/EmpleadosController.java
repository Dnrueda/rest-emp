package com.techuniversity.emp.controllers;

import com.techuniversity.emp.model.Empleado;
import com.techuniversity.emp.model.Empleados;
import com.techuniversity.emp.model.Formacion;
import com.techuniversity.emp.repositorios.EmpleadosDAO;
import com.techuniversity.emp.utils.Configuracion;
import com.techuniversity.emp.utils.Utilidades;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/empleados")
public class EmpleadosController {

    @Autowired
    private EmpleadosDAO empleadosDAO;

    @GetMapping(path="/", produces = "application/json")
    public Empleados getEmpleados(){
        return empleadosDAO.getAllEmpleados();
    }

    @GetMapping(path = "/{id}")
    public ResponseEntity<Empleado> getEmpleado(@PathVariable int id){
        Empleado emp = empleadosDAO.getEmpleado(id);
        if (emp != null) return new ResponseEntity<>(emp, HttpStatus.OK);
        else return ResponseEntity.notFound().build();
    }

    @PostMapping(path = "/", consumes = "application/json", produces = "application/json")
    public  ResponseEntity<Object> addEmpleado(@RequestBody Empleado emp){
        Integer id = empleadosDAO.getAllEmpleados().getListaEmpleados().size() + 1;
        emp.setId(id);
        empleadosDAO.addEmpleado(emp);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(emp.getId())
                .toUri();
        return  ResponseEntity.created(location).build();
    }

    //parte del control del update (para el primer metodo)
    @PutMapping(path = "/", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@RequestBody Empleado emp) {
        Empleado modEmpleado = empleadosDAO.updEmpleados(emp);
        if (modEmpleado == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().build();
        }
    }
    // segundo metodo del update
    @PutMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> updEmpleado(@PathVariable Integer id, @RequestBody Empleado emp) {
        Empleado modEmpleado = empleadosDAO.updEmpleados(id, emp);
        if (modEmpleado == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok().build();
        }
    }
    //borrado en el controlador
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Object> deleteEmpleado(@PathVariable Integer id){
        empleadosDAO.deleteEmpleado(id);
        return ResponseEntity.ok().build();
    }

    // PATCH en el controlador
    @PatchMapping(path = "/{id}", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Object> patchEmpleado(@PathVariable int id, @RequestBody Map<String,Object> updates) {
        Empleado emp = empleadosDAO.softupdEmpleado(id, updates);
        if (emp == null) {
            return ResponseEntity.notFound().build();
        } else{
            return ResponseEntity.ok().build();
        }
    }

    //funcion para mostrar solo formacion de un usuario
    @GetMapping(path = "/{id}/formaciones",produces = "application/json")
    public ResponseEntity<List<Formacion>> getFormacionesEmpleado(@PathVariable int id){
        List<Formacion> formaciones = empleadosDAO.getFormacionEmpleado(id);
        if (formaciones == null) {
            return ResponseEntity.notFound().build();
        } else{
            return ResponseEntity.ok().body(formaciones);
        }
    }

    //funcion para añadir formacion a un usuario
    @PostMapping(path = "/{id}/formaciones", consumes = "application/json", produces = "application/json")
    public  ResponseEntity<Object> addFormacionEmpleado(@PathVariable int id,@RequestBody Formacion formacion){
        Empleado emp = empleadosDAO.addFormacionEmpleado(id, formacion);
        if (emp == null){
            return ResponseEntity.notFound().build();
        }else{
            return ResponseEntity.ok().build();
        }
    }

    //aplicacion properties Formas de acceder

    @Value("${app.titulo}") private String titulo; //inyección del titulo
    @GetMapping("/titulo")
    public String getTitulo(){
        String modo = config.getModo();
        return titulo + " " + modo;
    }
// sirve para buscar la propiedad que quieras sin identificarla previamente a traves de variable

    @Autowired
    private Environment environment;

    @GetMapping("/prop")
    public String getPropiedad(@RequestParam("key") String key){
        String valor = "Sin valor";
        String keyValor = environment.getProperty(key);
        if (keyValor != null && !keyValor.isEmpty()) { //si es diferente de nulo y no este vacio
            valor = keyValor;
        }
        return valor;
    }

    //tercera forma de acceder a las propierties con la nueva clase configuracion

    @Autowired
    private Configuracion config;
    @GetMapping("/autor")
    public String getAutor() {
        return config.getAutor();
    }


    //para emepzar a tratar test
    @RequestMapping("/home")
    public String home(){
        return "a tope tiu";
    }

    //prueba para utilidad
    @GetMapping("/cadena")
    public String getCadena(@RequestParam String texto, @RequestParam String separador) {
        try {
            return Utilidades.getCadena(texto, separador);
        }catch (Exception ex){
            return ex.getMessage();
        }
    }
}
