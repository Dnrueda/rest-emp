package com.techuniversity.emp.repositorios;

import com.techuniversity.emp.model.Empleado;
import com.techuniversity.emp.model.Empleados;
import com.techuniversity.emp.model.Formacion;
import org.springframework.stereotype.Repository;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Repository
public class EmpleadosDAO {
    private static Empleados lista = new Empleados();
    static {
        Formacion form1 = new Formacion("2021/01/01","Curso Back");
        Formacion form2 = new Formacion("2020/01/01","Curso Front");
        Formacion form3 = new Formacion("2019/01/01","Curso SQL");
        ArrayList<Formacion> una = new ArrayList<Formacion>();
        una.add(form1);
        ArrayList<Formacion> dos = new ArrayList<Formacion>();
        dos.add(form1);
        dos.add(form2);
        ArrayList<Formacion> todas = new ArrayList<Formacion>();
        todas.add(form3);
        todas.addAll(dos);

        lista.getListaEmpleados().add(new Empleado(1, "Dani", "Rueda", "Dani@gmail.com",una));
        lista.getListaEmpleados().add(new Empleado(2, "Paula", "Ruda", "Paula@gmail.com",dos));
        lista.getListaEmpleados().add(new Empleado(3, "Carla", "Reda", "Carla@gmail.com",todas));
    }

    public Empleados getAllEmpleados(){
        return lista;
    }

    public Empleado getEmpleado(int id) {
        for (Empleado emp : lista.getListaEmpleados()){
            if (emp.getId() == id){
                return emp;
            }
        }
        return null;
    }
    public void addEmpleado(Empleado emp){
        lista.getListaEmpleados().add(emp);
    }

    //modificar empleados
    //pedimos 1 empleado
    public Empleado updEmpleados(Empleado emp){
        Empleado currentEmp = getEmpleado(emp.getId());
        if (currentEmp != null){
            currentEmp.setNombre(emp.getNombre());
            currentEmp.setApellidos(emp.getApellidos());
            currentEmp.setEmai(emp.getEmai());
        }
        return currentEmp;
    }
    //pedimos 1 empleado y un identificador
    public Empleado updEmpleados(int id, Empleado emp){
        Empleado currentEmp = getEmpleado(id);
        if (currentEmp != null){
            currentEmp.setNombre(emp.getNombre());
            currentEmp.setApellidos(emp.getApellidos());
            currentEmp.setEmai(emp.getEmai());
        }
        return currentEmp;
    }
    //delete de empleados por identificador
    public void deleteEmpleado(int id) {
        // para hacer bucle y q no se desfase al borrar
        Iterator itr = lista.getListaEmpleados().iterator();
        while (itr.hasNext()){
            Empleado emp = (Empleado)  itr.next();
            if (emp.getId() == id){
                itr.remove(); //se carga elemento que estoy itinerando
                break; //rompe el bucle y deja de iterar
            }
        }

    }

    //metodo PATCH
    public Empleado softupdEmpleado(int id, Map<String, Object> updates) {
        Empleado current = getEmpleado(id); //current es el empleado de la lista
        if (current != null){
            for (Map.Entry<String,Object> update : updates.entrySet()) {
                switch (update.getKey()) { //en función del campo de entrada modifica el q toca
                    case "nombre":
                        current.setNombre(update.getValue().toString());
                        break;
                    case "apellidos":
                        current.setApellidos(update.getValue().toString());
                        break;
                    case "emai":
                        current.setEmai(update.getValue().toString());
                        break;
                }
            }
        }
        return current;
    }
    //metodo para devolver la lista de formaciones de un empleado
    public List<Formacion> getFormacionEmpleado(int id) {
        for (Empleado emp : lista.getListaEmpleados()) {
            if (emp.getId() == id) {
                return emp.getFormaciones(); //despues de return se rompe el bucle
            }
        }
        return null; //si no lo encuentras damos null
    }

    //metodo para añadir formacion a un empleado
    public Empleado addFormacionEmpleado(int id, Formacion formacion) {
        Empleado current = getEmpleado(id);
        if (current != null){
            current.getFormaciones().add(formacion);//a la lista de colecciones le añado formacion nueva
        }
        return current;
    }
}
