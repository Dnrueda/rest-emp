package com.techuniversity.emp.model;

public class Formacion {

    //constructor vacio
    public Formacion() {
    }

    //constructor para montar formaciones
    public Formacion(String fecha, String titulo) {
        this.fecha = fecha;
        this.titulo = titulo;
    }

    private String fecha;
    private String titulo;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
}
