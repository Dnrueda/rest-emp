package com.techuniversity.emp.model;

import java.util.ArrayList;
import java.util.List;

public class Empleados {

    private List<Empleado> listaEmpleados;
    public List<Empleado> getListaEmpleados() {
        if (listaEmpleados == null) {
            listaEmpleados = new ArrayList<Empleado>();
        }
        return  listaEmpleados;
    }

    public void setListaEmpleados(List<Empleado> listaEmpleados) {
        this.listaEmpleados = listaEmpleados;
    }

}
