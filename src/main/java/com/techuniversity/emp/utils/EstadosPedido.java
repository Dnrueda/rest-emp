package com.techuniversity.emp.utils;

//para enumerar objetos y dependencias

public enum EstadosPedido {
    ACEPTADO,
    COCINADO,
    EN_ENTREGA,
    ENTREGADO,
    VALORADO,
    COMIDO;

}
