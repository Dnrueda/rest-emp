package com.techuniversity.emp.utils;

import java.util.Locale;

public class Utilidades {

    public static String getCadena(String texto, String separador) throws BadSeparator{
        if ((separador.length() > 1) || separador.trim().equals("")) throw new BadSeparator();
        char[] letras = texto.toUpperCase().toCharArray();
        String resultado = "";
        for (char letra: letras){
            if (letra != ' '){
                resultado += letra + separador;
            }else{
                if (!resultado.trim().equals("")){ //si el q t viene es espacio en blanco
                    resultado = resultado.substring(0, resultado.length() -1); //quita el punto previo al espacio
                }
                resultado += letra; // suma el espacio
            }
        }
        if (resultado.endsWith(separador)){
            resultado = resultado.substring(0, resultado.length() -1);//quitas punto del final
        }
        return resultado;
    }

    public static boolean espImpar(int numero){
        System.out.println(numero % 2);
        return (numero % 2 != 0); //evalua el resto 0 par 1 impar

    }


    public static boolean estaBlanco(String texto){
        return ((texto == null) || texto.trim().isEmpty()); //si esta a blanco o nulos dará true

    }

    //nueva función q apuntará a Estados del pedido

    public  static boolean valorarEstadoPedido(EstadosPedido estado){
        int valor = estado.ordinal(); //devuelve el numeor de enumeración
        return ((valor >= 0) && (valor <=5)); //comprobamos q estamos en alguno de los 5 estados
    }

}
