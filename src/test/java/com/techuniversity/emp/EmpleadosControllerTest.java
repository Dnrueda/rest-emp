package com.techuniversity.emp;


import com.techuniversity.emp.controllers.EmpleadosController;
import com.techuniversity.emp.utils.BadSeparator;
import com.techuniversity.emp.utils.EstadosPedido;
import com.techuniversity.emp.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;


@SpringBootTest
public class EmpleadosControllerTest {
    @Autowired
    EmpleadosController empleadosController;

    @Test
    public void TestCadena(){
        String esperado = "L.U.Z D.E.L S.O.L";
        assertEquals(esperado, empleadosController.getCadena("luz del sol", "."));
    }
    @Test
    public void testBadSeparator(){
        try {
            Utilidades.getCadena("bea cuesta", "..");
            fail("se esperaba Bad Separator");
        }catch (BadSeparator bs){}
    }

    @ParameterizedTest
    @ValueSource(ints = {1,3, 5 , -3, 15, Integer.MAX_VALUE})
    public void testEsImpar(int numero){
        assertTrue(Utilidades.espImpar(numero));
    }

    @ParameterizedTest
    @ValueSource(strings ={"", " "})
    public void testEsBlanco(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullSource //indica texto nulo
    @EmptySource //indica texto vacio
    public void testEstaBlancoNull(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings ={"", " ","\t","\n"})
    public void testEstaBlancoNull1(String texto){
        assertTrue(Utilidades.estaBlanco(texto));
    }

    //testeamos el estado de pedido
    @ParameterizedTest
    @EnumSource(EstadosPedido.class) //valores posibles los del enum
    public void testValorarEstadoPedido(EstadosPedido estado){
        assertTrue(Utilidades.valorarEstadoPedido(estado));
    }
}
